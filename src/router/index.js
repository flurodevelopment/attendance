import Vue from 'vue'
import Router from 'vue-router'
import store from '../store'
import RouterHistory from './history';

///////////////////////////////////

//Routes
const Home = () => import('./routes/Home.vue');
const Search = () => import('./routes/Search.vue');
const Event = () => import('./routes/Event.vue');




///////////////////////////////////

//User Authentication Routes
const UserLogin = () => import('./routes/UserLogin.vue');
const UserSignup = () => import('./routes/UserSignup.vue');
const UserForgot = () => import('./routes/UserForgot.vue');
const UserReset = () => import('./routes/UserReset.vue');
const UserAccounts = () => import('./routes/UserAccounts.vue');

///////////////////////////////////

//Content View Routes

//Generic View Route
const View = () => import('./routes/View.vue');



///////////////////////////////////

//Use the router
Vue.use(Router)

///////////////////////////////////


var array = [];

///////////////////////////////////

array.push({
    name: 'home',
    path: '/', //:view/:date',
    meta: {
        title: 'Calendar',
        requireUser: true,
        keepAlive: true,
        resetHistory:true,
    },
    props: (route) => ({
        defaultView: route.query.view,
        defaultDate: route.query.date,
    }),
    component: Home,
})

//////////////////////////////////////

array.push({
    name: 'event',
    path: '/event/:slug',
    meta: {
        title: 'Event',
        requireUser: true,
    },
    props: (route) => ({
        slug: route.params.slug,
    }),
    component: Event,
})

//////////////////////////////////////

array.push({
    name: 'user.login',
    path: '/user/login',
    meta: {
        title: 'Login',
        denyUser: true,
        search: {
            disabled: true,
        },
        resetHistory:true,
    },
    component: UserLogin,
})

array.push({
    name: 'user.signup',
    path: '/user/signup',
    meta: {
        title: 'Signup',
        denyUser: true,
        search: {
            disabled: true,
        },
        resetHistory:true,
    },
    component: UserSignup,
})

array.push({
    name: 'user.forgot',
    path: '/user/forgot',
    meta: {
        title: 'Forgot Password',
        denyUser: true,
        search: {
            disabled: true,
        },
        resetHistory:true,
    },
    component: UserForgot,
})

array.push({
    name: 'user.reset',
    path: '/user/reset',
    meta: {
        title: 'Reset Your Password',
        denyUser: true,
        disableHeader: true,
        disableFooter: true,
        search: {
            disabled: true,
        },
        resetHistory:true,
    },
    component: UserReset,
    props: (route) => ({
        token: route.query.token,
    })
})

array.push({
    name: 'user.accounts',
    path: '/user/accounts',
    meta: {
        title: 'My Accounts',
    },
    component: UserAccounts,
    meta: {
        requireUser: true,
        search: {
            disabled: true,
        },
        resetHistory:true,
    },
})

//////////////////////////////////////


array.push({
    name: 'search',
    path: '/search',
    meta: {
        title: 'Search',
        requireUser: true,
    },
    component: Search,
    props: (route) => ({
        keywords: route.query.keywords || '',
    })
})

///////////////////////////////////

var router = new Router({
    mode: 'history',
    routes: array,
    scrollBehavior(to, from, savedPosition) {

        // console.log('Save scroll position', from);
        //Keep track of where the user was scrolled
        //if they hit the back button
        var pos = 0;
        // Scroll to the top
        if (savedPosition) {
            pos = savedPosition.y;
        }
        document.body.scrollTop = document.documentElement.scrollTop = pos;
    },
});

///////////////////////////////////

router.beforeEach((to, from, next) => {
    
    //Close the drawer whenever we change route
    store.commit('ui/drawer', false)

    console.log('Route change now >')

    //////////////////////////////////////////////////

    if (to.meta) {
        //Get the user session from fluro
        var user = store.getters['fluro/user'];

        //If the route doesn't allow logged in users
        if (to.meta.requireUser) {
            if (user) {
                return next();
            }

            console.log('Store original intention')
            Vue.prototype.$fluro.global.intendedRoute = {
                name:to.name,
                params:to.params,
                query:to.query,
            }
            console.log('Route is only accessible to logged in users')
            return next(`/user/login?redirected=true`)
        }

        //If the route only allows logged in users
        if (to.meta.denyUser) {
            if (!user) {
                return next();
            }

            console.log('Route is not accessible to logged in users')
            return next(false)
        }
    }


    return next();
})

///////////////////////////////////

RouterHistory.init(router);



///////////////////////////////////

export default router;

///////////////////////////////////