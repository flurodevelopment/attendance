
import PagedContent from '@/components/PagedContent.vue';

export default {
	components:{
		PagedContent,
	},
	props:{
		queryPage:{
			type:Number,
			default:function() {
				return parseInt(this.$route.query.page) || 1;
			}
		}
	},
	data() {
		return {
			currentPage:this.queryPage,
		}
	},
    methods:{
    	pageChanged(index) {

    		this.currentPage = index;
    		//Leave all of the current router parameters and query details
    		//and update the ?page= in the url
            this.$router.replace({ query: Object.assign({}, this.$route.query, { page: index }) });
    	}
    }
}