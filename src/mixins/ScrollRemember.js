

var _ = require('lodash');

////////////////////////////////////////////

export default {
	data() {
		return {
			elements:{},
			positions:{},
			listeners:{},
			scrollRecording:false,
			// scrollListeners:{},
		}
	},
    methods:{
    	detachScrollListener(key) {

    		var self = this;

    		var element = self.elements[key];
    		if(!element) {
    			return;
    		}

    		//Remove the event listener
    		element.removeEventListener('scroll', self.listeners[key]);
    		self.elements[key] = null;			
    	},
    	attachScrollListener(element, key) {

            if(!element || !element.addEventListener) {
                console.log('No element provided', key, element);
                return;
            }

    		var self = this;

    		if(self.elements[key]) {
    			self.detachScrollListener(key);
    		}

    		self.elements[key] = element;

			//Create one now
			self.listeners[key] = function(event) {
				
				if(self.scrollRecording) {
					self.positions[key] = event.target.scrollTop;
				}
			}


			element.addEventListener('scroll', self.listeners[key]);
    	},
    	restorePositions() {
    		var self = this;
    		_.each(self.elements, function(element, key) {
    			element.scrollTop = self.positions[key];
    		})
    	}
    },
    deactivated() {
    	self.scrollRecording = false;
    },
    activated() {
    	var self = this;
        self.restorePositions();
    	self.scrollRecording = true;
    }
}